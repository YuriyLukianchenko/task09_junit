package com.lukianchenko.controller;

import com.lukianchenko.model.GameType;
import com.lukianchenko.model.Model;
import com.lukianchenko.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
    private Model model;
    private View view;
    private Logger logger = LogManager.getLogger(Controller.class);

    public void connectModel(Model model){
        this.model = model;
    }
    public void connectView(View view){
        this.view = view;
    }

    public void createGame(GameType game, Object obj){
        switch(game){
            case PLATEAU:
                model.createPlateauGame((int[])obj);
                break;
            case MINESWEEPER:
                model.createMinesweeperGame();
                break;
            default:
        }
    }

    public void showGameResult(GameType game){
        switch(game){
            case PLATEAU:
                model.plGame.findLongestPlateau();
                logger.info(model.plGame.getLongestPlateau());
                break;
            case MINESWEEPER:
                break;
            default:
        }
    }

}
