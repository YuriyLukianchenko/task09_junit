package com.lukianchenko.model;

import com.lukianchenko.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Model {
    private Controller controller;
    private Logger logger = LogManager.getLogger(Model.class);
    public PlateauGame plGame; //private
    public MinesweeperGame msGame; //private


    public void connectController(Controller controller){
        this.controller = controller;
    }

    public void createPlateauGame(int[] array){
        plGame = new PlateauGame(array);

        /*
        plGame = new PlateauGame(array);
        plGame.findSequences(array);
        logger.info("Sequences: ");
        plGame.sequences.stream().forEach(logger::info);
        plGame.plateauFilter(array, plGame.sequences);
        logger.info("Plateaus: ");
        plGame.plateaus.stream().forEach(logger::info);
        logger.info("longest plateau: ");
        plGame.longestPlateaus(plGame.plateaus);
        */
    }

    public void createMinesweeperGame(){
        msGame = new MinesweeperGame();
    }
}
