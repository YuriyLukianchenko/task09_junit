package com.lukianchenko.model;

public class Sequence {
    private int startIndex;
    private int finishIndex;
    private int length;

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getFinishIndex() {
        return finishIndex;
    }

    public void setFinishIndex(int finishIndex) {
        this.finishIndex = finishIndex;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void calculateLengthOfSequence(){
        length = finishIndex -startIndex + 1;
    }

    @Override
    public String toString(){
        StringBuilder str = new StringBuilder("sequence's parameters: ");
        return str.append("startIndex = ").append(startIndex).append(", finishIndex = ")
                .append(finishIndex).append(", length = ").append(finishIndex - startIndex + 1).toString();
    }
}
