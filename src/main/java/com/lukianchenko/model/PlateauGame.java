package com.lukianchenko.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class PlateauGame {
    private Logger logger = LogManager.getLogger(PlateauGame.class);
    private int[] array;
    private Sequence longestPlateau;
    public List <Sequence> plateaus; //private
    public List <Sequence> sequences; //private

    public Sequence getLongestPlateau() {
        return longestPlateau;
    }

    public PlateauGame(int[] array){
        this.array = array;
        this.longestPlateau = new Sequence();
        this.plateaus = new LinkedList<>();
        this.sequences = new LinkedList<>();
    }

    public Sequence findLongestPlateau(){
        findSequences(array);
        plateauFilter(array, sequences);
        longestPlateaus(plateaus);
        return longestPlateau;
    }




    public List<Sequence> findSequences(int[] array){ //private
        boolean issequence = false;
        Sequence sequence = new Sequence();
        int previousCell = array[0];
        int currentCell = array[0];
        for(int i = 1; i < array.length; i++ ){
            currentCell = array[i];
            if ((currentCell == previousCell)&&(issequence == false)) {
                issequence = true;
                sequence.setStartIndex(i-1);
                previousCell = array[i];
            } else if ((currentCell != previousCell)&&(issequence == true)) {
                issequence = false;
                sequence.setFinishIndex(i-1);
                sequence.calculateLengthOfSequence();
                sequences.add(sequence);
                sequence = new Sequence();
                previousCell = array[i];
            } else {
                previousCell = array[i];
            }
        }
        return sequences;
    }

    public List<Sequence> plateauFilter(int[] array, List<Sequence> sequences){//private
        for (int i = 0; i < sequences.size(); i++){
            int start = sequences.get(i).getStartIndex();
            int finish = sequences.get(i).getFinishIndex();
            if ((0 != start) // left border of array check
                    && ((array.length - 1) != finish) //right border of array check
                    && (array[start - 1] < array[start]) // left border of plateau check
                    && (array[finish + 1] < array[finish])) { // right border of plateau check
                plateaus.add(sequences.get(i));
            }
        }
        return plateaus;
    }

    /**
     * Return only first longest plateau (others plateaus with same longest length will be ignored).
     * @param plateaus - sequences.
     * @return sequence (plateau) with maximal length.
     */
    public Sequence longestPlateaus(List<Sequence> plateaus){//private
        longestPlateau = plateaus.stream().max((y , x) -> y.getLength() - x.getLength()).get();
        return longestPlateau;
    }
}
