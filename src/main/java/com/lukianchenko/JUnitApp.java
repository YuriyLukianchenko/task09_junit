package com.lukianchenko;

import com.lukianchenko.controller.Controller;
import com.lukianchenko.model.Model;
import com.lukianchenko.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Entry point of program.
 * @author Yura
 * @version 1.0
 */
public class JUnitApp {
    /**
     * Controller  of MVC patern.
     */
    private Controller controller;
    /**
     * Model  of MVC patern.
     */
    private Model model;
    /**
     * View  of MVC patern.
     */
    private View view;
    /**
     * Constructor with initialization of controller, view and model.
     */
    public JUnitApp(Controller controller,Model model, View view){
        this.controller = controller;
        this.model = model;
        this.view = view;
    }

    /**
     * Main method, entry point.
     * @param args arguments from console.
     */
    public static void main( final String[] args) {
        Logger logger = LogManager.getLogger(JUnitApp.class);

        JUnitApp app = new JUnitApp(new Controller(), new Model(), new View());
        app.start();
    }
    /**
     * Method which starts the program.
     */
    public void start(){
        view.connectController(controller);
        controller.connectModel(model);
        controller.connectView(view);
        model.connectController(controller);
        view.start();

    }
}
