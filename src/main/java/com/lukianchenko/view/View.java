package com.lukianchenko.view;

import com.lukianchenko.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


import static com.lukianchenko.model.GameType.PLATEAU;

public class View {
    private Controller controller;
    private Logger logger = LogManager.getLogger(View.class);
    private Scanner scanner = new Scanner(System.in);

    public void connectController(Controller controller){
        this.controller = controller;
    }

    public void start(){
        String option;
        label:
        while(true){
            showMainOptions();
            option = readOption();
            switch(option){
                case "1":
                    innerLabel1:
                    while(true){
                        showPlateauOptions();
                        option = readOption();
                        switch(option){
                            case "1":
                                logger.info("enter integer numbers separated by space");
                                String[] line = scanner.nextLine().split(" ");
                                int[] array = new int[line.length];
                                for(int i = 0; i < line.length; i++){
                                    array[i] = Integer.parseInt(line[i]);
                                }
                                //Arrays.stream(array).forEach(logger::info);
                                controller.createGame(PLATEAU,array);
                                logger.info("array was loaded");
                                break;
                            case "2":
                                int[] array2 = new int[100];// need to be in model
                                Random rand = new Random();// need to be in model
                                for(int i = 0; i < 100; i++){// need to be in model
                                    array2[i] = rand.nextInt(5);// need to be in model
                                }
                                controller.createGame(PLATEAU,array2);
                                logger.info("array was generated");
                                break;
                            case "3":
                                controller.showGameResult(PLATEAU);
                                break;
                            case "4":
                            default:
                                break innerLabel1;
                        }
                    }
                    break;
                case "2":

                    innerLabel2:
                    while(true){
                        showMinesweeperOptions();
                        option = readOption();
                        switch(option){
                            case "1":
                                break;
                            case "2":
                                break;
                            case "3":
                                break;
                            case "4":
                            default:
                                break innerLabel2;
                        }
                    }
                    break;
                case "3":
                    break label;
                default:

            }
        }


    }

    private void showMainOptions(){
        logger.info("Menu:");
        logger.info("1. Plateau Game");
        logger.info("2. Minesweeper Game");
        logger.info("3. Quit");
    }

    private String readOption(){
        return scanner.nextLine();
    }

    private void showPlateauOptions(){
        logger.info("1. load array of integer numbers");
        logger.info("2. generate array (100 elements and 5 values)");
        logger.info("3. find longest plateau");
        logger.info("4. quit");
    }

    private void showMinesweeperOptions(){
        logger.info("1. input initial data");
        logger.info("2. show crop with mines");
        logger.info("3. show crop after mines defusing");
        logger.info("4. quit");

    }
}
