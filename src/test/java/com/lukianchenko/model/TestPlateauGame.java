package com.lukianchenko.model;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPlateauGame {
    public int[] createInt(){
        int[] array = new int[10];
        array[0] = 1;
        array[1] = 2;
        array[2] = 2;
        array[3] = 2;
        array[4] = 4;
        array[5] = 4;
        array[6] = 1;
        array[7] = 5;
        array[8] = 5;
        array[9] = 1;
        return array;
    }

    @Test
    public void testFindSequences(){
        PlateauGame plGame = new PlateauGame(createInt());
        List<Sequence> list = plGame.findSequences(createInt());
        assertEquals(3,list.size());
        assertEquals(1, list.get(0).getStartIndex());
        assertEquals(3,list.get(0).getFinishIndex());
        assertEquals(3,list.get(0).getLength());
        assertEquals(4, list.get(1).getStartIndex());
        assertEquals(5,list.get(1).getFinishIndex());
        assertEquals(2,list.get(1).getLength());
        assertEquals(7, list.get(2).getStartIndex());
        assertEquals(8,list.get(2).getFinishIndex());
        assertEquals(2,list.get(2).getLength());

    }

}
